import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:get_it/get_it.dart';

import 'share_store.dart';

class DynamicDetailGetIt extends StatelessWidget {
  DynamicDetailGetIt({Key? key}) : super(key: key) {
    GetIt.I.registerSingleton<ShareStore>(ShareStore());
  }

  @override
  Widget build(BuildContext context) {
    print('build');
    return Scaffold(
      appBar: AppBar(
        title: Text('局部 Store'),
      ),
      body: Stack(
        children: [
          Container(height: 300, color: Colors.red),
          Positioned(
            bottom: 0,
            height: 60,
            width: MediaQuery.of(context).size.width,
            child: Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                _PraiseButton(),
                _FavorButton(),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class _FavorButton extends StatelessWidget {
  const _FavorButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    print('FavorButton');
    return Container(
      alignment: Alignment.center,
      color: Colors.blue,
      child: TextButton(
        onPressed: () {
          GetIt.I.get<ShareStore>().increamentFavor();
        },
        child: Observer(builder: (context) {
          print('FavorButton Observer');
          return Text(
            '收藏 ${GetIt.I.get<ShareStore>().favorCount}',
            style: TextStyle(color: Colors.white),
          );
        }),
        style: ButtonStyle(
            minimumSize: MaterialStateProperty.resolveWith(
                (states) => Size((MediaQuery.of(context).size.width / 2), 60))),
      ),
    );
  }
}

class _PraiseButton extends StatelessWidget {
  const _PraiseButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    print('PraiseButton');
    return Container(
      alignment: Alignment.center,
      color: Colors.green[400],
      child: TextButton(
        onPressed: () {
          GetIt.I.get<ShareStore>().increamentPraise();
        },
        child: Observer(
          builder: (context) => Text(
            '点赞 ${GetIt.I.get<ShareStore>().praiseCount}',
            style: TextStyle(color: Colors.white),
          ),
        ),
        style: ButtonStyle(
            minimumSize: MaterialStateProperty.resolveWith(
                (states) => Size((MediaQuery.of(context).size.width / 2), 60))),
      ),
    );
  }
}
