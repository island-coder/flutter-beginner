import 'package:mobx/mobx.dart';

part 'share_store.g.dart';

class ShareStore = ShareStoreBase with _$ShareStore;

abstract class ShareStoreBase with Store {
  @observable
  int praiseCount = 0;

  @observable
  int favorCount = 0;

  @action
  void increamentPraise() {
    praiseCount++;
  }

  @action
  void increamentFavor() {
    favorCount++;
  }
}
