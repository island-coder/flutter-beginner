import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../bloc/personal_bloc.dart';
import '../models/personal_entity.dart';

class PersonalHomePageRepository extends StatelessWidget {
  PersonalHomePageRepository({Key? key}) : super(key: key);
  final personalBloc = PersonalBloc(
      PersonalResponse(
        personalProfile: null,
        status: LoadingStatus.loading,
      ),
      userId: '70787819648695');

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<PersonalBloc, PersonalResponse>(
      bloc: personalBloc,
      builder: (_, personalResponse) {
        print('build PersonalHomePage');
        if (personalResponse.status == LoadingStatus.loading) {
          return Center(
            child: Text('加载中...'),
          );
        }
        if (personalResponse.status == LoadingStatus.failed) {
          return Center(
            child: Text('请求失败'),
          );
        }
        PersonalEntity personalProfile = personalResponse.personalProfile!;
        return Stack(
          children: [
            RepositoryProvider.value(
              child: CustomScrollView(
                slivers: [
                  const BannerWithAvatar(),
                  const PersonalProfile(),
                  const PersonalStatistic(),
                ],
              ),
              value: personalProfile,
            ),
            Positioned(
              top: 40,
              right: 10,
              child: IconButton(
                onPressed: () {
                  personalBloc.add(FetchEvent());
                },
                icon: Icon(
                  Icons.refresh,
                  color: Colors.white,
                ),
              ),
            ),
          ],
        );
      },
      buildWhen: (previous, next) {
        if (previous.personalProfile == null || next.personalProfile == null) {
          return true;
        }
        return true;
        //return previous.personalProfile!.userId != next.personalProfile!.userId;
      },
    );
  }
}

class BannerWithAvatar extends StatelessWidget {
  final double bannerHeight = 230;
  final double imageHeight = 180;
  final double avatarRadius = 45;
  final double avatarBorderSize = 4;

  const BannerWithAvatar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SliverToBoxAdapter(
      child: Container(
        height: bannerHeight,
        color: Colors.white70,
        alignment: Alignment.topLeft,
        child: Stack(
          children: [
            Container(
              height: bannerHeight,
            ),
            Positioned(
              top: 0,
              left: 0,
              child: CachedNetworkImage(
                imageUrl:
                    'https://ss1.bdstatic.com/70cFvXSh_Q1YnxGkpoWK1HF6hhy/it/u=688497718,308119011&fm=26&gp=0.jpg',
                height: imageHeight,
                width: MediaQuery.of(context).size.width,
                fit: BoxFit.fill,
              ),
            ),
            Positioned(
              left: 20,
              top: imageHeight - avatarRadius - avatarBorderSize,
              child: _getAvatar(
                context.read<PersonalEntity>().avatar,
                avatarRadius * 2,
                avatarBorderSize,
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _getAvatar(String avatarUrl, double size, double borderSize) {
    return Stack(alignment: Alignment.center, children: [
      Container(
        width: size + borderSize * 2,
        height: size + borderSize * 2,
        clipBehavior: Clip.antiAlias,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(size / 2 + borderSize),
        ),
      ),
      Container(
        width: size,
        height: size,
        clipBehavior: Clip.antiAlias,
        decoration: BoxDecoration(
          color: Colors.black,
          borderRadius: BorderRadius.circular(size / 2),
        ),
        child: CachedNetworkImage(
          imageUrl: avatarUrl,
          height: size,
          width: size,
          fit: BoxFit.fill,
        ),
      ),
    ]);
  }
}

class PersonalProfile extends StatelessWidget {
  const PersonalProfile({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SliverToBoxAdapter(
      child: Container(
        padding: EdgeInsets.fromLTRB(20, 10, 20, 0),
        color: Colors.white70,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Text(
                  context.read<PersonalEntity>().userName,
                  style: TextStyle(fontSize: 18.0),
                ),
                SizedBox(
                  width: 3,
                ),
                _getLevel(context.read<PersonalEntity>().level),
              ],
            ),
            SizedBox(height: 2),
            Text(
              context.read<PersonalEntity>().jobDescription,
              style: TextStyle(fontSize: 15.0, color: Colors.grey[700]),
            ),
            SizedBox(height: 20),
            Text(
              context.read<PersonalEntity>().description,
              style: TextStyle(fontSize: 14.0, color: Colors.grey[700]),
            ),
          ],
        ),
      ),
    );
  }

  Widget _getLevel(String level) {
    return Container(
      padding: EdgeInsets.all(2.0),
      decoration: BoxDecoration(
        color: Colors.blue[300],
        borderRadius: BorderRadius.all(Radius.circular(2.0)),
      ),
      child: Text(
        level,
        style: TextStyle(color: Colors.white, fontSize: 12),
      ),
    );
  }
}

class PersonalStatistic extends StatelessWidget {
  const PersonalStatistic({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SliverToBoxAdapter(
      child: Container(
        color: Colors.white70,
        alignment: Alignment.topLeft,
        padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
        child: const StatisticContent(),
      ),
    );
  }
}

class StatisticContent extends StatelessWidget {
  const StatisticContent({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        _getStatisticDesc('关注', context.read<PersonalEntity>().followeeCount),
        SizedBox(
          width: 20,
        ),
        _getStatisticDesc('关注者', context.read<PersonalEntity>().followerCount),
        SizedBox(
          width: 20,
        ),
        _getStatisticDesc('掘力值', context.read<PersonalEntity>().power),
      ],
    );
  }

  Widget _getStatisticDesc(String itemName, int count) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          count.toString(),
          style: TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.bold,
          ),
        ),
        Text(
          itemName,
          style: TextStyle(
            fontSize: 13.0,
            color: Colors.grey[700],
          ),
        ),
      ],
    );
  }
}
