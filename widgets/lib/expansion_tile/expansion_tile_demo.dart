import 'package:flutter/material.dart';

class ExpansionTileDemo extends StatefulWidget {
  const ExpansionTileDemo({Key? key}) : super(key: key);

  @override
  State<ExpansionTileDemo> createState() => _ExpansionTileDemoState();
}

class _ExpansionTileDemoState extends State<ExpansionTileDemo> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('ShaderMask'),
        backgroundColor: Colors.red[400]!,
      ),
      body: Center(
        child: ListView.builder(
          itemBuilder: (_, index) {
            return const DynamicWithComment();
          },
          itemCount: 10,
        ),
      ),
    );
  }
}

class DynamicWithComment extends StatefulWidget {
  const DynamicWithComment({Key? key}) : super(key: key);

  @override
  State<DynamicWithComment> createState() => _DynamicWithCommentState();
}

class _DynamicWithCommentState extends State<DynamicWithComment> {
  bool _expanded = false;

  @override
  Widget build(BuildContext context) {
    return ExpansionTile(
      onExpansionChanged: (expanded) {
        setState(() {
          _expanded = expanded;
        });
      },
      title: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Image.asset('images/girl.jpeg', width: 80.0),
          const SizedBox(width: 8.0),
          const Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  '这是标题',
                  style: TextStyle(
                    fontSize: 16.0,
                  ),
                  textAlign: TextAlign.left,
                ),
                SizedBox(height: 8.0),
                Text(
                  '这是说明文字',
                  style: TextStyle(fontSize: 14.0, color: Colors.grey),
                  textAlign: TextAlign.left,
                ),
              ],
            ),
          ),
        ],
      ),
      initiallyExpanded: _expanded,
      // 展开后的 title 组件的 text 颜色（未指定颜色时）
      textColor: Colors.red,
      // 展开后背景色
      backgroundColor: Colors.blue[50],
      //收起状态背景色
      collapsedBackgroundColor: Colors.grey[50],
      trailing: Icon(
          _expanded
              ? Icons.keyboard_arrow_up_outlined
              : Icons.keyboard_arrow_down_outlined,
          color: Colors.grey),
      expandedAlignment: Alignment.centerLeft,
      expandedCrossAxisAlignment: CrossAxisAlignment.start,
      childrenPadding: EdgeInsets.zero,
      children: [
        Container(
          width: double.infinity,
          margin: const EdgeInsets.fromLTRB(20.0, 10.0, 10.0, 0.0),
          padding: const EdgeInsets.all(5.0),
          color: Colors.grey[400],
          child: const Text('xx1: 评论'),
        ),
        Container(
          width: double.infinity,
          margin: const EdgeInsets.fromLTRB(20.0, 10.0, 10.0, 0.0),
          padding: const EdgeInsets.all(5.0),
          color: Colors.grey[400],
          alignment: Alignment.centerRight,
          child: const Text('xx2: 评论'),
        ),
        Container(
          width: double.infinity,
          margin: const EdgeInsets.fromLTRB(20.0, 10.0, 10.0, 10.0),
          padding: const EdgeInsets.all(5.0),
          color: Colors.grey[400],
          child: const Text('xx3: 评论'),
        ),
      ],
    );
  }
}
