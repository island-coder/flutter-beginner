class ShoppingItem {
  final String name;
  final bool selected;
  final int count;

  ShoppingItem({required this.name, this.selected = false, this.count = 1});

  bool operator ==(Object? other) {
    if (other == null || !(other is ShoppingItem)) return false;
    return other.name == this.name &&
        other.selected == this.selected &&
        other.count == this.count;
  }

  @override
  get hashCode => this.toJson().hashCode;

  Map<String, String> toJson() {
    return {
      'name': name,
      'selected': selected.toString(),
      'count': count.toString(),
    };
  }

  factory ShoppingItem.fromJson(Map<String, dynamic> json) {
    return ShoppingItem(
      name: json['name']!,
      selected: json['selected'] == 'true',
      count: int.parse(json['count']!),
    );
  }
}
