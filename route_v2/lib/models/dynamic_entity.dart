import 'dart:convert';

class DynamicEntity {
  String _title;
  String _imageUrl;
  int _viewCount;
  int _id;

  get title => _title;
  get imageUrl => _imageUrl;
  get viewCount => _viewCount;
  get id => _id;

  static DynamicEntity fromJson(Map<String, dynamic> json) {
    DynamicEntity newEntity = DynamicEntity();
    newEntity._id = json['id'];
    newEntity._title = json['title'];
    newEntity._imageUrl = json['imageUrl'];
    newEntity._viewCount = json['viewCount'];

    return newEntity;
  }

  Map<String, dynamic> toJson() {
    return {
      'id': _id,
      'title': _title,
      'imageUrl': _imageUrl,
      'viewCount': _viewCount
    };
  }
}
