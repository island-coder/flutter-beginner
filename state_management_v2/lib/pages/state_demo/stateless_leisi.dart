import 'package:flutter/material.dart';
import 'package:home_framework/states/face_emotion.dart';

class StatelessLeisi extends StatelessWidget {
  final FaceEmotion face;
  StatelessLeisi({required this.face, key}) : super(key: key) {
    print('setState=====constructor: 雷思');
  }

  @override
  Widget build(BuildContext context) {
    print('setState=====build：雷思');
    return Center(
      child: Text('雷思感受到了小芙的${face.emotion}'),
    );
  }
}
