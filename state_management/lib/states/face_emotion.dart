class FaceEmotion {
  final String emotion;
  const FaceEmotion({this.emotion = '平静'});

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;
    if (other.runtimeType != runtimeType) {
      return false;
    }
    final FaceEmotion otherModel = other;
    return otherModel.emotion == emotion;
  }

  @override
  int get hashCode => emotion.hashCode;
}
