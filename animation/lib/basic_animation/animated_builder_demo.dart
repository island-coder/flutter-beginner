import 'dart:math';

import 'package:flutter/material.dart';

class RotationSwitchAnimatedBuilder extends StatelessWidget {
  final Widget child1, child2;
  final Animation<double> animation;
  const RotationSwitchAnimatedBuilder(
      {Key? key,
      required this.animation,
      required this.child1,
      required this.child2})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: animation,
      builder: (context, child) {
        if (animation.value < 0.5) {
          return Transform(
            transform: Matrix4.identity()
              ..rotateZ(animation.value * pi)
              ..setEntry(0, 1, -0.003),
            alignment: Alignment.center,
            child: child1,
          );
        } else {
          return Transform(
            transform: Matrix4.identity()
              ..rotateZ(pi)
              ..rotateZ(animation.value * pi)
              ..setEntry(1, 0, 0.003),
            child: child2,
            alignment: Alignment.center,
          );
        }
      },
    );
  }
}

class AnimatedBuilderDemo extends StatefulWidget {
  const AnimatedBuilderDemo({Key? key}) : super(key: key);

  @override
  _AnimatedBuilderDemoState createState() => _AnimatedBuilderDemoState();
}

class _AnimatedBuilderDemoState extends State<AnimatedBuilderDemo>
    with SingleTickerProviderStateMixin {
  late Animation<double> animation;
  late AnimationController controller;

  @override
  void initState() {
    super.initState();
    controller =
        AnimationController(duration: const Duration(seconds: 1), vsync: this);
    animation = Tween<double>(begin: 0, end: 1.0).animate(controller);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('AnimatedBuilder 动画'),
      ),
      body: RotationSwitchAnimatedBuilder(
        animation: animation,
        child1: Center(
          child: Container(
            padding: EdgeInsets.all(10),
            margin: EdgeInsets.all(10),
            constraints: BoxConstraints(minWidth: double.infinity),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(4.0),
              gradient: LinearGradient(
                colors: [
                  Colors.orange,
                  Colors.green,
                ],
              ),
            ),
            child: Text(
              '点击按钮变出小姐姐',
              style: TextStyle(
                fontSize: 20,
                color: Colors.white,
                fontWeight: FontWeight.bold,
              ),
              textAlign: TextAlign.center,
            ),
          ),
        ),
        child2: Center(
          child: Image.asset('images/beauty.jpeg'),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.play_arrow, color: Colors.white),
        onPressed: () {
          if (controller.status == AnimationStatus.completed) {
            controller.reverse();
          } else {
            controller.forward();
          }
        },
      ),
    );
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }
}

class AnimatedBuilderDemo1 extends StatefulWidget {
  const AnimatedBuilderDemo1({Key? key}) : super(key: key);

  @override
  _AnimatedBuilderDemo1State createState() => _AnimatedBuilderDemo1State();
}

class _AnimatedBuilderDemo1State extends State<AnimatedBuilderDemo1>
    with SingleTickerProviderStateMixin {
  late Animation<double> animation;
  late AnimationController controller;

  @override
  void initState() {
    super.initState();
    controller =
        AnimationController(duration: const Duration(seconds: 1), vsync: this);
    animation = Tween<double>(begin: 0, end: 1.0).animate(controller);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('AnimatedBuilder 动画'),
      ),
      body: RotationSwitchAnimatedBuilder(
        animation: animation,
        child1: Center(
          child: Container(
            width: 100,
            height: 100,
            color: Colors.blue,
          ),
        ),
        child2: Center(
          child: ClipOval(
            child: Container(
              width: 100,
              height: 100,
              color: Colors.blue,
            ),
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.play_arrow, color: Colors.white),
        onPressed: () {
          if (controller.status == AnimationStatus.completed) {
            controller.reverse();
          } else {
            controller.forward();
          }
        },
      ),
    );
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }
}
